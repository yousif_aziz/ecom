<?php

use Carbon\Carbon;
use Faker\Factory;
use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $num = 10;
        $new = Carbon::now();
        $facker = Factory::create();

        $items = [];
        for ($I = 0; $I < $num; $I++) {
            $items[] = [
                "email" => $facker->email,
                "first_name" => $facker->firstName,
                "last_name" => $facker->lastName,
                "store_credit" => $facker->numberBetween(200,2000),
                "created_at" => $new,
                "updated_at" => $new,
            ];
        }

        Customer::insert($items);

    }
}
