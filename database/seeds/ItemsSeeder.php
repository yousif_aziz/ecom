<?php

use App\Models\Item;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $num = 100;
        $new = Carbon::now();
        $facker = Factory::create();
//        dd($facker->text(600), $facker->words(2));
        $items = [];
        for ($I = 0; $I < $num; $I++) {
            $items[] = [
                "price" => $facker->randomFloat(2, 50, 3000),
                "name" => implode(" ", $facker->words(2)),
                "description" => $facker->text(600),
                "created_at" => $new,
                "updated_at" => $new,
            ];
        }

        Item::insert($items);

    }
}
