<!doctype html>
<html lang="en" ng-app="eCom">
<head>

    <base href="/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Album example · Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/album/">

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <!--    <link href="album.css" rel="stylesheet">-->
</head>
<body>
<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="/" class="navbar-brand d-flex align-items-center">

                <strong>E-com</strong>
            </a>
            <!--            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"-->
            <!--                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">-->
            <!--                <span class="navbar-toggler-icon"></span>-->
            <!--            </button>-->
        </div>
    </div>
</header>

<main role="main">

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">E-commerce Demo Proposal </h1>
            <p class="lead text-muted">
                this web for demo proposal
            </p>
            <p>
                <a href="/" class="btn btn-primary my-2">Home</a>
                <a href="/cart" class="btn btn-secondary my-2">Cart</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <ng-view></ng-view>
    </div>

</main>


<style>
    .cart-btn {
        position: fixed;
        left: 0;
        border: none;
        outline: none;
        box-shadow: 1px 1px 7px #0000002b;
        bottom: 51px;
        background: #86c7ff;
    }

    .num {
        position: absolute;
        background: #fd5d5d;
        padding: 0px 6px;
        border-radius: 10px;
        color: #fff;
        top: 17px;
        left: 40px;
    }
</style>
<div ng-controller="cartViewer">
    <a class="cart-btn" href="/cart">
        <img src="https://upload.wikimedia.org/wikipedia/commons/6/67/Ic_shopping_cart_48px.svg">
<!--        <span class="num">{{// Object.keys().length }}</span>-->
    </a>
</div>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            Powerd by Yousif Aziz
        </p>
    </div>
</footer>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="/assets/js/bootstrap.bundle.min.js"
        integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o"
        crossorigin="anonymous"></script>


<script src="/assets/lib/sw.js"></script>
<script src="/assets/lib/anguler.js"></script>
<script src="/assets/lib/angular-route.min.js"></script>
<script src="/app/App.js"></script>
</body>
</html>
