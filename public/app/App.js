const Settings = {
    routes: () => {
        return {
            "/": {temp: "items-list", controller: "itemList"},
            "/cart": {temp: "cart", controller: "cart"},
            "/checkout": {temp: "checkout", controller: "checkout"}
        }
    }
}

const eCom = ((Settings) => {

    let app = angular.module("eCom", ["ngRoute"]);

    app.config(function ($routeProvider, $locationProvider) {

        for (let route in Settings.routes()) {
            let currentRoute = Settings.routes()[route];
            currentRoute.templateUrl = `/app/templates/${currentRoute.temp}.html`;
            $routeProvider.when(route, currentRoute);

        }

        $routeProvider.otherwise({
            templateUrl: "/app/templates/404.html"
        });

        $locationProvider.html5Mode(true);
    });

    app.run(($rootScope) => {


        $rootScope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase === '$apply' || phase === '$digest') {
                if (fn && (typeof (fn) === 'function'))
                    fn();
            } else
                this.$apply(fn);
        };


    });


    return app;
})(Settings);

eCom.service("itemListService", function ($rootScope) {

    return {
        get: (params = {}) => {
            return axios.get('/api/items', {params})
        }
    }


});


eCom.service("cartListService", function ($rootScope) {

    return {
        get: (params = {}) => {
            return axios.get('/api/cart', {params})
        },
        total: () => {
            return axios.get('/api/cart/total')
        },
        checkout: (data) => {
            return axios.post('/api/cart/checkout', data)
        },
        delete: id => {
            return axios.delete(`/api/cart/${id}`)

        }
    }


});

eCom.service("cart", function ($rootScope) {
    const self = {items: {}};


    self.init = (data) => {

        Object.keys(data).forEach(slug => {
            self.items[slug] = data[slug];
        })

    }


    self.add = (item) => {

        if (self.isExists(item))
            self.items[item]++;
        else {
            self.items[item] = 1;
        }

        axios.post('/api/cart/add', {item_slug: item, quantity: 1})

        return self.getCount(item);

    }


    self.deduct = item => {

        if (self.isExists(item))
            self.items[item]--;

        axios.post('/api/cart/add', {item_slug: item, quantity: 1, type: "deduct"})
        return self.getCount(item);
    }


    self.isExists = item => {
        return !!self.items[item]
    }

    self.getCount = (item) => {
        return self.items[item] || 0;
    }

    return self;


});

eCom.controller("itemList", function ($scope, itemListService, cart) {

    $scope.itemsList = [];
    $scope.cartList = {};


    const initCart = itemList => {
        let newLis = {};
        itemList.forEach(item => {
            // console.log(item.slug, item.cartCount)
            if (item.cartCount > 0) {
                newLis[item.slug] = item.cartCount;
                $scope.cartList[item.slug] = item.cartCount;
            }

        })

        cart.init(newLis);

    }

    const fetchItems = (filter = {}) => {
        itemListService.get(filter).then(response => {
            $scope.safeApply(() => {
                // swal('Hello world!')
                $scope.meta = response.data.meta;
                $scope.itemsList = $scope.itemsList.concat(response.data.data);

                initCart(response.data.data)

            })
        });
    }


    $scope.more = () => {


        if ($scope.meta.current_page < $scope.meta.last_page) {
            fetchItems({page: $scope.meta.current_page + 1})
        } else {
            swal("sorry no more data")
        }

    }


    $scope.addToCart = itemSlug => {

        // swal("item add to cart")
        $scope.cartList[itemSlug] = cart.add(itemSlug);
    }


    $scope.removeFromCart = itemSlug => {
        ;
        // swal("item removed from cart")
        $scope.cartList[itemSlug] = cart.deduct(itemSlug);
    }

// -------------------
    fetchItems();
})


eCom.controller("cartViewer", function ($scope, cart) {
    $scope.num = cart.items;
})

eCom.controller("cart", function ($scope, cartListService, cart) {

    $scope.cartList = [];
    $scope.quantityList = {};

    const calcTotal = () => {
        let total = 0;
        $scope.cartList.forEach(item => {
            total += ($scope.quantityList[item.item.slug] * item.item.price)
        })

        $scope.total = total.toFixed(2);
    }

    const removeFromList = item => {
        let index = $scope.cartList.indexOf(item);
        if (index !== -1) {
            $scope.cartList.splice(index, 1)
            calcTotal();
        }
    }
    const initCart = itemList => {
        let newLis = {};
        itemList.forEach(item => {
                newLis[item.item.slug] = item.quantity;
                $scope.quantityList[item.item.slug] = item.quantity;
            }
        );
        // console.log(newLis)
        cart.init(newLis);
    }


    cartListService.get().then(response => {
        $scope.safeApply(function () {
            $scope.cartList = response.data
            initCart($scope.cartList)
            calcTotal()
        })
    })


    $scope.addToCart = itemSlug => {

        // swal("item add to cart")
        $scope.quantityList[itemSlug] = cart.add(itemSlug);
        console.log($scope.quantityList)
        calcTotal();
    }


    $scope.removeFromCart = itemSlug => {

        // swal("item removed from cart")

        if (cart.getCount(itemSlug) < 2) {
            swal("sorry item quantity must be at lease 1 ");
            return
        }

        $scope.quantityList[itemSlug] = cart.deduct(itemSlug);
        console.log($scope.quantityList)
        calcTotal();

    };


    $scope.remove = item => {
        cartListService.delete(item.id).then(res => {
          $scope.safeApply(function () {
              if (res.data.status) {
                  swal("deleted successfully")
                  removeFromList(item);
              } else {

                  swal("some thing wrong happen")
              }
          })
        })
    }

})


eCom.controller("checkout", function ($scope, cartListService, $location) {

    $scope.total = 0;

    $scope.form = {};


    cartListService.total().then(response => {
        $scope.safeApply(function () {
            if (response.data.total === 0) {
                swal("sorry you don`t items in cart");
                $location.path("/");

            }
            $scope.total = response.data.total
        })
    })


    $scope.checkout = () => {

        cartListService.checkout($scope.form)
            .then(res => {
                $scope.safeApply(function () {

                    if (res.data.errors) {
                        showErrors(res.data.errors)
                    }

                    if (res.data.status) {
                        swal("thank you");
                        $location.path("/");
                    }

                })
            })


    }


})
;


const showErrors = function (errors) {

    let text = "";
    for (let err in errors) {
        let currentError = errors[err];
        for (let i in currentError) {
            text += currentError[i] + "\n";
        }

    }
    swal(text);
}
