<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router =;
/** @var \Laravel\Lumen\Routing\Router $router */


$router->group(["prefix" => "api"], function () use ($router) {

    $router->get('/items', "ItemsController@index");
    $router->get('/cart', "CartController@index");
    $router->post('/cart/add', "CartController@store");
    $router->get('/cart/total', "CartController@total");
    $router->post('/cart/checkout', "CartController@checkout");
    $router->delete('/cart/{id}', "CartController@delete");
});


$router->get('/{route:.*?}', function () {
    return view('main');
});


