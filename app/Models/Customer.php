<?php


namespace App\Models;


use App\Http\Contracts\HaveBalance;

/**
 * @property mixed store_credit
 *
 * @property mixed id
 */
class Customer extends BaseModel implements HaveBalance
{

    public function haveEnoughBalance(float $amount): bool
    {
        return $this->getBalance() >= $amount;
    }

    public function getBalance(): float
    {
        return floatval($this->store_credit);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function deductBalance(float $amount): bool
    {

        $this->store_credit = $this->store_credit - $amount;
        $this->save();
        return true;
        // TODO: Implement deductBalance() method.
    }
}