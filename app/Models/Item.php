<?php


namespace App\Models;


class Item extends BaseModel
{

    public function scopeFilter($q, array $dataToSearch)
    {
        $q->when(@$dataToSearch["name"], function ($q) use ($dataToSearch) {

            $q->where("name", "like", "%" . $dataToSearch["name"] . " % ");

        });


    }


    public function cartItems()
    {
        return $this->hasMany(Cart::class,"item_id");
    }

}