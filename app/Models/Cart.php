<?php


namespace App\Models;


class Cart extends BaseModel
{
    protected $table = "cart";
    protected $guarded = [];

    public static function clear(int $userId)
    {
        self::whereCustomerId($userId)->delete();
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

}