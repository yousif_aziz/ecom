<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{


    public static function quickSave($data)
    {
        return (new static)->create(collect($data)->toArray());
    }


    public static function upsart(array $fields, array $data = [])
    {
        /** @var self $row */
        $row = static::firstOrNew($fields);

        foreach ($data as $k => $v) {
            $row->{$k} = $v;
        }

        $row->save();

        return $row;
    }

    /**3
     * @param $data
     * @return static
     */
    public static function quickUpdate(array $data)
    {
        $row = (new static)->findOrFail($data['id']);
        $row->update($data);
        return $row;
    }


}