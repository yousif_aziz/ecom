<?php


namespace App\Http\Controllers;


use App\Http\Resources\ItemResource;
use App\Http\Services\ItemsServices;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{

    public function index(ItemsServices $itemsServices, Request $request)
    {
        return ItemResource::collection($itemsServices->filter($request->all())->with(["cartItems" => function ($q) {
            $q->whereCustomerId(1);
        }])->paginate(6));
    }


}