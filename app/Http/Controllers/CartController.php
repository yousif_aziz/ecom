<?php


namespace App\Http\Controllers;


use App\Http\RequestForms\AddToCartRequestValidator;
use App\Http\RequestForms\CheckOutValidator;
use App\Http\Resources\CartResource;
use App\Http\Services\CartServices;
use App\Http\Services\PaymentMethods\CustomerCredit;
use App\Models\Cart;
use App\Models\Customer;

class CartController extends Controller
{

    public function delete(CartServices $services, $id)
    {


        return response()->json($services->delete($id));

    }

    public function checkout(CheckOutValidator $validator)
    {

        $address = $validator->request()->get("address");
        $telephone = $validator->request()->get("telephone");

        try {
            CartServices::checkout(new CustomerCredit(), 1, $address, $telephone);
            return response()->json(["status" => true]);
        } catch (\Exception $e) {
            return response()->json(["errors" => [[$e->getMessage()]]]);
        }


    }

    public function total()
    {
        return response()->json(["total" => CartServices::getTotalAmount(1)]);

    }

    public function index()
    {

        return response()->json(CartResource::collection(Cart::all()));

    }

    public function store(AddToCartRequestValidator $req, CartServices $cartServices)
    {

        $customer_id = 1;
        $quantity = $req->request()->get("quantity");
        $deduct = $req->request()->get("type") === "deduct";
        $item_id = explode("-", $req->request()->get("item_slug"))[0];


        $services = $cartServices->make($customer_id, $item_id);

        if ($deduct)
            $result = $services->deductAndSave($quantity);
        else {
            $result = $services->addMoreAndSave($quantity);
        }

        return response()->json(["type" => ($deduct) ? "deduct" : "add", "result" => $result]);


    }


}