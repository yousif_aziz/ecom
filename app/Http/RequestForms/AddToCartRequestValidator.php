<?php


namespace App\Http\RequestForms;


class AddToCartRequestValidator extends BaseRequestForm
{

    public function rules(): array
    {
        return ["item_slug" => "required|max:255", "quantity" => "required|numeric"];
    }

    public function authorized(): bool
    {
        return true;
    }
}