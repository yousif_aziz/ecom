<?php


namespace App\Http\RequestForms;


class CheckOutValidator extends BaseRequestForm
{

    public function rules(): array
    {

        return [
            "address"=>"required|max:255",
            "telephone"=>"required|numeric",
        ];
        // TODO: Implement rules() method.
    }

    public function authorized(): bool
    {
        return true;
    }
}