<?php


namespace App\Http\Contracts;


interface HaveBalance
{

    public function getId(): int;

    public function haveEnoughBalance(float $amount): bool;

    public function deductBalance(float $amount): bool;

    public function getBalance(): float;


}