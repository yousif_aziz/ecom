<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class ItemResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'cartCount' => $this->cartItems[0]->quantity??0,
            'slug' => $this->id . "-" . Str::slug($this->name),
//            'description' => $this->description,
            'price' => $this->price,
            'mini_description' => Str::words($this->description,10),
        ];
    }
}