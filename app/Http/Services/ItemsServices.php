<?php


namespace App\Http\Services;


use App\Models\Item;
use Illuminate\Database\Eloquent\Builder;

class ItemsServices
{
    /**
     * @var Item
     */
    private $model;


    /**
     * ItemsServices constructor.
     * @param Item $model
     */
    public function __construct(Item $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $dataToSearch
     * @return Builder
     */
    public function filter(array $dataToSearch)
    {
        return $this->model->filter($dataToSearch);
    }
}