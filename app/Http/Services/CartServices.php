<?php


namespace App\Http\Services;


use App\Http\Contracts\HaveBalance;
use App\Http\Services\PaymentMethods\PaymentMethodInterface;
use App\Models\Cart;
use App\Models\Customer;
use App\Models\Order;
use DB;

class CartServices
{

    /**
     * @var Cart
     */
    private $model;
    /**
     * @var int
     */
    private $customer_id;
    private $item_id;

    public function __construct(Cart $model)
    {
        $this->model = $model;
    }

    public static function getTotalAmount(int $userId)
    {
        $total = 0;
        $items = Cart::whereCustomerId($userId)->with("item")->get();
        foreach ($items as $item) {
            $total += ($item->quantity * $item->item->price);
        }
        return $total;


    }

    public static function checkout(PaymentMethodInterface $method, $customerId, $address = "", $telephone = "")
    {

        $amount = self::getTotalAmount($customerId);


        if ($method->handel(Customer::find($customerId), $amount)) {
            Order::quickSave([
                "address" => $address,
                "telephone" => $telephone,
                "customer_id" => $customerId,
                "total" => $amount,
            ]);
        }

    }

    public function create(int $userId, array $data)
    {

        $data = collect($data)->only("item_slug", "quantity");


        $this->model->item_id = explode("-", $data["item_slug"])[0];
        $this->model->quantity = $data["quantity"];
        $this->model->customer_id = $userId;
        $this->model->save();

        return $this->model;


    }

    /**
     * @param int $customer_id
     * @param $item_id
     * @return CartServices
     */
    public function make(int $customer_id, $item_id)
    {
        $this->item_id = $item_id;
        $this->customer_id = $customer_id;
        return $this;
    }

    public function addMoreAndSave($num)
    {
        $row = $this->model->whereCustomerId($this->customer_id)
            ->whereItemId($this->item_id)
            ->first();

        if ($row) {
            $row->quantity = $row->quantity + abs($num);
        } else {
            $row = $this->model;
            $row->customer_id = $this->customer_id;
            $row->item_id = $this->item_id;
            $row->quantity = abs($num);
        }
        $row->save();
        return $row;
    }

    public function deductAndSave($num)
    {
        $row = $this->model->whereCustomerId($this->customer_id)
            ->whereItemId($this->item_id)
            ->first();

        if ($row) {
            $row->quantity = $row->quantity - abs($num);

            if ($row->quantity <= 0) {
                $row->delete();
                return;
            } else {
                $row->save();
                return $row;
            }

        }
    }

    public function delete($id)
    {
//        dd(Cart::all());
        $row = $this->model->find($id);
        if ($row) {
            $row->delete();
            return ["status" => true];
        }
        return ["status" => false];
    }


}
