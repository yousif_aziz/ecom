<?php


namespace App\Http\Services\PaymentMethods;


use App\Http\Contracts\HaveBalance;
use App\Models\Cart;
use App\Models\Order;
use DB;

class CustomerCredit implements PaymentMethodInterface
{


    /**
     * @param HaveBalance $customer
     * @param float $amount
     * @return bool
     * @throws \Exception
     */
    public function handel(HaveBalance $customer, float $amount): bool
    {


        if ($customer->haveEnoughBalance($amount)) {

            DB::beginTransaction();

            $customer->deductBalance($amount);
            Cart::clear($customer->getId());
            DB::commit();
            return true;
        }

        throw new \Exception("sorry you dont have enough balance");
    }
}