<?php


namespace App\Http\Services\PaymentMethods;


use App\Http\Contracts\HaveBalance;

interface  PaymentMethodInterface
{

    public function handel(HaveBalance $customer, float $amount): bool;

}